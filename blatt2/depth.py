def ost(pos):
    return (pos[0]+1,pos[1])

def nord(pos):
    return (pos[0],pos[1]-1)

def west(pos):
    return (pos[0]-1,pos[1])

def süd(pos):
    return (pos[0],pos[1]+1)

def valid(pos):
    return pos[0] in range(7) and pos[1] in range(3)

goal = (5,1)
limit = 8
visitednodes = 0 
def search(pos,depth):
    if not valid(pos) or depth >= limit:
        return False
    print(depth,pos)
    global visitednodes
    visitednodes += 1
    if pos == goal:
        return True
    return search(ost(pos),depth+1) or search(nord(pos),depth+1) or search(west(pos),depth+1) or search(süd(pos),depth+1)


search((6,0),0)
print(visitednodes)